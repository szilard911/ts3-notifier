--
-- Testmodule callback functions
--
-- To avoid function name collisions, you should use local functions and export them with a unique package name.
--
local pmMyNam = "PMWatch "

function onTextMessageEvent(serverConnectionHandlerID, targetMode, toID, fromID, fromName, fromUniqueIdentifier, message, ffIgnored)
    print(pmMyNam .. ": onTextMessageEvent: " .. serverConnectionHandlerID .. " " .. targetMode .. " " .. toID .. " " .. fromID .. " " .. fromName .. " " .. fromUniqueIdentifier .. " " .. message .. " " .. ffIgnored)
	return 0
end

function onClientPokeEvent(serverConnectionHandlerID, pokerID, pokerName, message, ffIgnored)
    print(pmMyNam .. "Lua: onClientPokeEvent: " .. serverConnectionHandlerID .. " " .. pokerID .. " " .. pokerName .. " " .. message .. " " .. ffIgnored)
	return 0
end

pmwatch_events = {
	onTextMessageEvent = onTextMessageEvent,
	onClientPokeEvent  = onClientPokeEvent,
}
