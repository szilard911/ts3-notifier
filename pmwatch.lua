local VERSION = "2.5.0 Growl/Snarl type !pmhelp for usage"
local MYPGM = "PMWatch"

local toggle_pmpoke = 1;
local toggle_pmwatch = 1;
local toggle_growl = 1;
local runstr ="";

pmwatchids = {}
pmsoundf = {}

----------------------------------------------------

function onTextMessageEvent(serverConnectionHandlerID, targetMode, toID, fromID, fromName, fromUniqueIdentifier, message, ffIgnored)

--    ts3.printMessage(serverConnectionHandlerID, "AMI-on-text-message: ", 0)

    if (pmwTextMessageEvent ~= nil) then
      pmwTextMessageEvent(serverConnectionHandlerID, targetMode, toID, fromID, fromName, fromUniqueIdentifier, message, ffIgnored)
    end
    if (amiTextMessageEvent ~= nil) then
      amiTextMessageEvent(serverConnectionHandlerID, targetMode, toID, fromID, fromName, fromUniqueIdentifier, message, ffIgnored)
    end
    if (lnwTextMessageEvent ~= nil) then
      lnwTextMessageEvent(serverConnectionHandlerID, targetMode, toID, fromID, fromName, fromUniqueIdentifier, message, ffIgnored)
    end
	  return 0
end

----------------------------------------------------

function pmwTextMessageEvent(serverConnectionHandlerID, targetMode, toID, fromID, fromName, fromUniqueIdentifier, message, ffIgnored)

    local valueAWAY, error = ts3.getClientVariableAsInt (serverConnectionHandlerID, ts3.getClientID(serverConnectionHandlerID), ts3defs.ClientProperties.CLIENT_AWAY)	
   	local pmwUniqueID, error = ts3.getClientVariableAsString(serverConnectionHandlerID, fromID, ts3defs.ClientProperties.CLIENT_UNIQUE_IDENTIFIER)
	  local ourclientid, error = ts3.getClientID (serverConnectionHandlerID)	

    local playit = 1
  
    if valueAWAY == 1 then  -- Don't play sound if our client is set away
       playit = 0
    end
  
    if fromID == ourclientid then  -- Don't play sound if the message is from ourselves
      playit = 0
       
      pmwCheckCommand(serverConnectionHandlerID, fromID, ourclientid, message)
    end
  
    if (toggle_pmwatch == 0) then
      return 0
    end
  
    if targetMode ~= 1 then       -- Don't play sound if it is not a private message
      playit = 0
    end
     
    if playit == 1 then       -- Play sound 
    
      if (pmsoundf[pmwUniqueID] == nil) then
        pmwUniqueID = "default"
      end

      ts3.playWaveFile(serverConnectionHandlerID, pmsoundf[pmwUniqueID])
--      print("PMW: GotPM " )
    end

	  return 0
end

----------------------------------------------------

function onClientPokeEvent(serverConnectionHandlerID, pokerID, pokerName, message, ffIgnored)

    if (pmwClientPokeEvent ~= nil) then
      pmwClientPokeEvent(serverConnectionHandlerID, pokerID, pokerName, message, ffIgnored)
    end
    if (amiClientPokeEvent ~= nil) then
      amiClientPokeEvent(serverConnectionHandlerID, pokerID, pokerName, message, ffIgnored)
    end
 	  return 0
end

----------------------------------------------------

function pmwClientPokeEvent(serverConnectionHandlerID, pokerID, pokerName, message, ffIgnored)

	  local valueAWAY, error = ts3.getClientVariableAsInt (serverConnectionHandlerID, ts3.getClientID(serverConnectionHandlerID), ts3defs.ClientProperties.CLIENT_AWAY)	
  	local pmwUniqueID, error = ts3.getClientVariableAsString(serverConnectionHandlerID, pokerID, ts3defs.ClientProperties.CLIENT_UNIQUE_IDENTIFIER)
	  local ourclientid, error = ts3.getClientID (serverConnectionHandlerID)	

    local playit = 1
  
    if (toggle_pmwatch == 0) then
      return 0
    end
  
    if valueAWAY == 1 then  -- Don't play sound if our client is set away
       playit = 0
    end
  
    if pokerID == ourclientid then  -- Don't play sound if the message is from ourselves
       playit = 0
    end
  
    if playit == 1 then       -- Play sound 

      if (pmsoundf[pmwUniqueID] == nil) then
        pmwUniqueID = "default"
      end

      ts3.playWaveFile(serverConnectionHandlerID, pmsoundf[pmwUniqueID])
    end
  
    if (toggle_pmpoke == 0) then
      return 0
    end

	  local mapping = {}
	  mapping["é"] = "�" 
	  mapping["á"] = "�"
	  mapping["í"] = "�"
	  mapping["ű"] = "�"
	  mapping["ő"] = "�"
	  mapping["ú"] = "�"
	  mapping["ö"] = "�"
	  mapping["ü"] = "�"
	  mapping["ó"] = "�"

	  mapping["Á"] = "�"
--rv
	  mapping["Í"] = "�"
--zt
	  mapping["Ű"] = "�"
--r
	  mapping["Ő"] = "�"
--t
	  mapping["Ü"] = "�"
--k
	  mapping["Ö"] = "�"
--rf
	  mapping["Ú"] = "�"
--r
	  mapping["Ó"] = "�"
--g
	  mapping["É"] = "�" 
--p

	  
	  local omsg = utf8replace (message, mapping)
	  
----------- Snarl Code -----------
	if toggle_growl ~= 1 then
	  SnarlRegMsg = "(\"".. ts3.getPluginPath() .."lua_plugin\\z05pmwatch\\heysnarl.exe\" register^?app^-sig^=app^/ts3notify\"&\"keep-alive=1\"&\"uid=1122334455\"&\"password=811\"&\"icon=".. ts3.getPluginPath() .."lua_plugin\\z05pmwatch\\icon.png\"&\"title=\"TS3 Notify\")"
	  SnarlSendMsg ="(\"".. ts3.getPluginPath() .."lua_plugin\\z05pmwatch\\heysnarl.exe\"" .. " notify^?app^-sig^=app^/ts3notify^&password^=811^&uid=1122334455^&title^=POKE:".. string.format('%q',pokerName) .."^&text^=".. string.format('%q',omsg) ..")"
	  --ts3.requestSendPrivateTextMsg(serverConnectionHandlerID, "[b][color=green]Poke from[/color] [color=blue]".. SnarlRegMsg.."&"..SnarlSendMsg .."[/b]", ourclientid)
	  os.execute(SnarlRegMsg.."&"..SnarlSendMsg)
	
	else

------------ Growl Code
	
	  --GrowlExe = "\"cmd /c \" \"" .. ts3.getPluginPath() .. "lua_plugin\\z05pmwatch\\growlnotify.exe" .. "\""
	  --GrowlExe = "\"cmd /c\" \"" .. ts3.getPluginPath() .. "lua_plugin\\z05pmwatch\\growlnotify.com" .. "\""
	  GrowlExe = "\"" .. ts3.getPluginPath() .. "lua_plugin\\z05pmwatch\\growlnotify.exe" .. "\""
	  GrowlAppIcon = " ^/ai:\"" .. ts3.getPluginPath() .. "lua_plugin\\z05pmwatch\\icon.png\""
	  GrowlPokeMessage = " ^/r:\"Poke\" ^/a:\"TS3\" ^/n:\"Poke\" ^/s:1 ^/t:\"Poke - " .. pokerName .. "\" " .. string.format('%q',omsg)
	  GrowlMessage = "(" ..GrowlExe .. GrowlAppIcon .. GrowlPokeMessage .. ")"
	  
	  -- ts3.requestSendPrivateTextMsg(serverConnectionHandlerID, GrowlMessage , ourclientid)
	  os.execute(GrowlMessage)
	end
	
	  --local ToTxtCmd = "echo \"".. message .. " --- " .. omsg .. "\" >> c:\\tstempfile.txt"
	  --ts3.requestSendPrivateTextMsg(serverConnectionHandlerID, ToTxtCmd , ourclientid)
	  --os.execute(ToTxtCmd)
	  
------------

	  ts3.requestSendPrivateTextMsg(serverConnectionHandlerID, "[b][color=green]Poke from[/color] [color=blue]"..pokerName.. ":".."[/color] "..message.."[/b]", ourclientid)
-- 	  print("PMW: GotPoke " )
--	  runstr = "C:\\Growl\\growlnotify.exe " .. " /t:" .. "\"POKE:\"" .. " " .. string.format('%q',pokerName) .." /p:2 /s:1 ".. string.format('%q',osmessage)
-- 	  os.execute(runstr)

 	  return 0
end




----------------------------------------------------

function pmwCheckCommand(serverConnectionHandlerID, fromID, ourclientid, message)

    local pmwCommands = { "!pmhelp", "!pmstatus", "!togglepmwatch", "!togglepokemsg", "!pmwconfig","!pmsetgrowl","!pmsetsnarl" }
    
    if fromID ~= ourclientid then  -- Don't check if the message is not from ourselves
      return 0
    end

-------
    if string.find(message, pmwCommands[1]) then   -- !help Display help/usage text

      ts3.printMessageToCurrentTab(" ")
      ts3.printMessageToCurrentTab(" [b]PMWatch Commands[/b]: ")
      ts3.printMessageToCurrentTab(" [b] !pmhelp           [/b]: display this help text")
      ts3.printMessageToCurrentTab(" [b] !pmstatus         [/b]: show status of toggles")
      ts3.printMessageToCurrentTab(" [b] !togglepmwatch  [/b]: toggle pmwatch script on/off, and reads pmwatch.ini")
      ts3.printMessageToCurrentTab(" [b] !togglepokemsg  [/b]: toggle write of poke message to chat tab on or off. off=just play sound")
      ts3.printMessageToCurrentTab(" [b] !pmwconfig      [/b]: Open notepad.exe on pmwatch.ini, to configure pmwatch")
      ts3.printMessageToCurrentTab(" [b] !pmsetgrowl      [/b]: Set Growl as your notifier application (must be installed) (default)")
      ts3.printMessageToCurrentTab(" [b] !pmsetsnarl      [/b]: Set Snarl as your notifier application (must be installed)")
      return 0
    end
-------
    if string.find(message, pmwCommands[2]) then   -- !status - Show status of toggles

       pmwmsg = "On "
       togmsg = "On "
       if (toggle_pmpoke ~= 1) then
         togmsg = "Off"
       end

       if (toggle_pmwatch ~= 1) then
         pmwmsg = "Off"
       end
	   
	   if (toggle_growl ~= 1) then
         pmalerter = "Snarl"
	   else
	     pmalerter = "Growl"
       end

       ts3.printMessageToCurrentTab(" PMWatch Status: ")
       ts3.printMessageToCurrentTab(" pmwatch   : " .. pmwmsg)
       ts3.printMessageToCurrentTab(" togglepokemsg  : " .. togmsg)
	   ts3.printMessageToCurrentTab(" notifier app  : " .. pmalerter)
      return 0
    end
-------
    if string.find(message, pmwCommands[4]) then   -- !togglepokemsg - Toggle write of poke message to chat tab on or off

      if (toggle_pmpoke ~= 1) then
        togmsg = "On "
        toggle_pmpoke = 1
      else
        togmsg = "Off"
        toggle_pmpoke = 0
      end

      ts3.printMessageToCurrentTab(" togglepokemsg  : " .. togmsg)
      return 0
    end
-------
    if string.find(message, pmwCommands[3]) then   -- !togglepmwatch - Toggle pmwatch script on/off

      if (toggle_pmwatch ~= 1) then
        togmsg = "On "
        toggle_pmwatch = 1
        pmwatchon (ts3.getCurrentServerConnectionHandlerID())
      else
        togmsg = "Off"
        toggle_pmwatch = 0
      end

      ts3.printMessageToCurrentTab(" pmwatch  : " .. togmsg)
      return 0
    end
-------
    if string.find(message, pmwCommands[5]) then   -- !pmwconfig - Open notepad.exe on pmwatch.ini, to configure pmwatch

      pmwconfig()
      return 0
    end
-------

-------
    if string.find(message, pmwCommands[6]) then   -- "!pmsetgrowl" set messager to growl
        togmsg = "Growl "
        toggle_growl = 1;
      ts3.printMessageToCurrentTab(" Alerter  : " .. togmsg)
      return 0
    end
-------

-------
    if string.find(message, pmwCommands[7]) then   -- "!pmsetsnarl" set messager to snarl

        togmsg = "Snarl "
        toggle_growl = 0;
      ts3.printMessageToCurrentTab(" Alerter  : " .. togmsg)
      return 0
    end
-------



end

----------------------------------------------------

function pmwatchon(serverConnectionHandlerID)

   local ourclientid, error = ts3.getClientID (serverConnectionHandlerID)	

   toggle_pmwatch = 1
   
   pmsoundf["editor"] = "notepad.exe"
   pmsoundf["default"] = "plugins/pmwatch/mysounds/gotpm.wav"

   fn = "plugins/pmwatch/mysounds/gotpm.wav"
   fh = io.open(fn, "r")

   if fh then
       fh:close()
   else
      ts3.requestSendPrivateTextMsg(serverConnectionHandlerID, "[b][color=red]Warning: ".. fn .." Does not exist, Default Sounds will NOT play [/color][/b]", ourclientid )
      ts3.requestSendPrivateTextMsg(serverConnectionHandlerID, "AppPath: " .. ts3.getAppPath() .. " ", ourclientid)
      ts3.requestSendPrivateTextMsg(serverConnectionHandlerID, "ResourcePath: " .. ts3.getResourcesPath() .. " ", ourclientid)
   end

   fn = "plugins/pmwatch/pmwatch.ini"
   fh = io.open(fn, "r")

   if fh then
   while true do
       line = fh.read(fh)
       if not line then break end
       tt = fromCSV (line)
       line2 = ""

       if tt[1] == "1" then
          pmsoundf[tt[2]] = "plugins/pmwatch/mysounds/" .. tt[3]
--          ts3.printMessage(serverConnectionHandlerID, "Data: " .. pmsoundf[tt[2]] .. " ", 0)

          fn2 = pmsoundf[tt[2]]
          fh2 = io.open(fn2, "r")

          if fh2 then
             fh2:close()
          else
             ts3.requestSendPrivateTextMsg(serverConnectionHandlerID, "[b][color=red]Warning: ".. fn2 .." does not exist, This sound will not play [/color][/b]", ourclientid )
--             ts3.requestSendPrivateTextMsg(serverConnectionHandlerID, "AppPath: " .. ts3.getAppPath() .. " ", ourclientid)
--             ts3.requestSendPrivateTextMsg(serverConnectionHandlerID, "ResourcePath: " .. ts3.getResourcesPath() .. " ", ourclientid)
          end
       end
   end
       fh:close()
   else
      ts3.requestSendPrivateTextMsg(serverConnectionHandlerID, "[b][color=red]Warning: ".. fn .." Does not exist, Using Defaults [/color][/b]", ourclientid )
      ts3.requestSendPrivateTextMsg(serverConnectionHandlerID, "AppPath: " .. ts3.getAppPath() .. " ", ourclientid)
      ts3.requestSendPrivateTextMsg(serverConnectionHandlerID, "ResourcePath: " .. ts3.getResourcesPath() .. " ", ourclientid)
   end
   

end

----------------------------------------------------

function pmwconfig()

    ts3.printMessageToCurrentTab(" Opening: pmwatch.ini with " .. pmsoundf["editor"] .. " Warning: All teamspeak events paused")
    os.execute(pmsoundf["editor"] .. " plugins/pmwatch/pmwatch.ini")
 	  return 0
end

----------------------------------------------------

function RemoveOldCode()

--   
--   With OneClick install, I had to move the directory where things were installed
--   To make it easy to remove the old locations, this routine will delete the files in the old location
--   

    PLUGINDIR = "./plugins/lua_plugin/zpmwatch/"
    os.remove (PLUGINDIR .. "init.lua")
    os.remove (PLUGINDIR .. "events.lua")
    os.remove (PLUGINDIR .. "pmwatch.lua")
    os.remove (PLUGINDIR)

    SOUNDDIR = "./mysounds/"
    os.remove (SOUNDDIR .. "gotpm.wav")
    os.remove (SOUNDDIR .. "gotpm-bell.wav")
    os.remove (SOUNDDIR .. "hfreqsnd.wav")
    os.remove (SOUNDDIR .. "moved.wav")
    os.remove (SOUNDDIR .. "moved-boop.wav")
    os.remove (SOUNDDIR .. "moved-dwoop.wav")
    os.remove (SOUNDDIR .. "pmwatch1.wav")
    os.remove (SOUNDDIR .. "pmwatch2.wav")
    os.remove (SOUNDDIR .. "pmwatch3.wav")
    os.remove (SOUNDDIR .. "sgt.wav")
    os.remove (SOUNDDIR .. "status.wav")
    os.remove (SOUNDDIR .. "status-blip.wav")
    os.remove (SOUNDDIR .. "pmwatch.ini")
    os.remove (SOUNDDIR)
    
    os.remove ("install-pmwatch.txt")
    os.remove ("xxx")

 	  return 0
end

----------------------------------------------------

function fromCSV (s)
  s = s .. ','        -- ending comma
  local t = {}        -- table to collect fields
  local fieldstart = 1
  repeat
    -- next field is quoted? (start with `"'?)
    if string.find(s, '^"', fieldstart) then
      local a, c
      local i  = fieldstart
      repeat
        -- find closing quote
        a, i, c = string.find(s, '"("?)', i+1)
      until c ~= '"'    -- quote not followed by quote?
      if not i then error('unmatched "') end
      local f = string.sub(s, fieldstart+1, i-1)
      table.insert(t, (string.gsub(f, '""', '"')))
      fieldstart = string.find(s, ',', i) + 1
    else                -- unquoted; find next comma
      local nexti = string.find(s, ',', fieldstart)
      table.insert(t, string.sub(s, fieldstart, nexti-1))
      fieldstart = nexti + 1
    end
  until fieldstart > string.len(s)
  return t
end

----------------------------------------------------

-----------------------------------------------------------------------------------------------------------
-- Print Message that Script has been loaded.
-----------------------------------------------------------------------------------------------------------

 CSCHID = ts3.getCurrentServerConnectionHandlerID()
 local mystatus, error = ts3.getConnectionStatus(CSCHID) 
 local server, srverror = ts3.getServerVariableAsString(CSCHID, ts3defs.VirtualServerProperties.VIRTUALSERVER_UNIQUE_IDENTIFIER)
 local ourclientid, error = ts3.getClientID(CSCHID)	
 
 ts3.requestSendPrivateTextMsg(CSCHID, "[b][color=blue]".. MYPGM .." Version " .. VERSION .. " [/color][/b]", ourclientid )
 --ts3.requestSendPrivateTextMsg(CSCHID, "[b][color=blue]"..  ts3.getPluginPath() .. " [/color][/b]", ourclientid )

 pmwatchon (CSCHID)

 RemoveOldCode()
 
 